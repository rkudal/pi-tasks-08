#include "Exceptions.h"
#include <iostream>
using std::cout;
using std::endl;

OUT_OF_RANGE::OUT_OF_RANGE(int index, int length)
{
	this->index = index;
	this->length = length;
}

void OUT_OF_RANGE::info()
{
	cout << "-----OUT OF RANGE-----\n" << endl;
	cout << "Used wrong index: " << index << endl;
	cout << "Vector's length: " << length << endl;
	cout << "Available diapason: 0 - " << length - 1 << endl;
	cout << "\n----------------------" << endl;

}

DIF_LENGTH::DIF_LENGTH(int fl, int sl)
{
	firstLength = fl;
	secondLength = sl;
}

void DIF_LENGTH::info()
{
	cout << "-VECTORS LENGTHS ARENT EQUAL-\n" << endl;
	cout << "First vector's length: " << firstLength << endl;
	cout << "Second vector's length: " << secondLength << endl;
	cout << "\n-----------------------------" << endl;
}

DIF_SIZES::DIF_SIZES(int fr, int fc, int sr, int sc)
{
	firstRows = fr;
	firstColumns = fc;
	secondRows = sr;
	secondColumns = sc;
}

void DIF_SIZES::info()
{
	cout << "\n--------------------------" << endl;
	cout << "MATRICES SIZES ARENT EQUAL\n" << endl;
	cout << "First matrix size: " << firstRows << " x " << firstRows << endl;
	cout << "Second matrix size: " << secondRows << " x " << secondColumns << endl;
	cout << "--------------------------" << endl;
}
