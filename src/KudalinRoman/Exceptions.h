#pragma once

struct OUT_OF_RANGE
{
	int index;
	int length;
	OUT_OF_RANGE(int index, int length);
	void info();
};

struct DIF_LENGTH
{
	int firstLength;
	int secondLength;
	DIF_LENGTH(int fl, int sl);
	void info();
};

struct DIF_SIZES
{
	int firstRows;
	int firstColumns;
	int secondRows;
	int secondColumns;
	DIF_SIZES(int fr, int fc, int sr, int sc);
	void info();
};