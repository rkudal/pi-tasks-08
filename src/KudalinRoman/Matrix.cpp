#include "Matrix.h"
#include "Exceptions.h"
#include <fstream>
#include <string>
using namespace std;

Matrix::Matrix()
{
	rowsNum = columnsNum = 0;
	rows = nullptr;
}

Matrix::Matrix(int rowsNum, int columnsNum) : rowsNum(rowsNum), columnsNum(columnsNum), 
rows(new Vector[rowsNum])
{
	for (int i = 0; i < rowsNum; i++)
		rows[i] = Vector(columnsNum);
}

Matrix::Matrix(int rowsNum, int columnsNum, double filler) : rowsNum(rowsNum), columnsNum(columnsNum), 
rows(new Vector[rowsNum])
{
	for (int i = 0; i < rowsNum; i++)
		rows[i] = Vector(columnsNum, filler);
}

Matrix::Matrix(const Matrix& matr)
{
	rowsNum = matr.rowsNum;
	columnsNum = matr.columnsNum;
	rows = new Vector[rowsNum];
	for (int i = 0; i < rowsNum; i++)
		rows[i] = Vector(matr.rows[i]);
}

Matrix::~Matrix()
{
	if (rows)
		delete[] rows;
	rows = nullptr;
}

void Matrix::random()
{
	for (int i = 0; i < rowsNum; i++)
		for (int j = 0; j < columnsNum; j++)
			rows[i][j] = rand() % 101;
}

ostream& operator<<(ostream& stream, const Matrix& matr)
{
	for (int i = 0; i < matr.rowsNum; i++)
		stream << matr.rows[i];
	return stream;
}

istream& operator>>(istream& stream, Matrix& matr)
{
	cout << "Enter " << matr.rowsNum << " matrix rows:" << endl;
	for (int i = 0; i < matr.rowsNum; i++)
		stream >> matr.rows[i];
	return stream;
}

Matrix Matrix::operator+(const Matrix& matr)
{
	try
	{
		if (rowsNum != matr.rowsNum || columnsNum != matr.columnsNum)
		{
			DIF_SIZES exc(rowsNum, columnsNum, matr.rowsNum, matr.columnsNum);
			throw exc;
		}
	}
	catch (DIF_SIZES exc)
	{
		exc.info();
		exit(EXIT_FAILURE);
	}
	Matrix result(rowsNum, columnsNum);
	for (int i = 0; i < rowsNum; i++)
		result.rows[i] = rows[i] + matr.rows[i];
	return result;
}

Matrix Matrix::operator-(const Matrix& matr)
{
	try
	{
		if (rowsNum != matr.rowsNum || columnsNum != matr.columnsNum)
		{
			DIF_SIZES exc(rowsNum, columnsNum, matr.rowsNum, matr.columnsNum);
			throw exc;
		}
	}
	catch (DIF_SIZES exc)
	{
		exc.info();
		exit(EXIT_FAILURE);
	}
	Matrix result(rowsNum, columnsNum);
	for (int i = 0; i < rowsNum; i++)
		result.rows[i] = rows[i] - matr.rows[i];
	return result;
}

Matrix Matrix::operator*(const Matrix& matr)
{
	if (columnsNum != matr.rowsNum)
	{
		cout << "MATRICES CANNOT BE MULTIPLIED" << endl;
		exit(EXIT_FAILURE);
	}
	Matrix result(rowsNum, matr.columnsNum, 0);
	for (int i = 0; i < rowsNum; i++)
		for (int j = 0; j < matr.columnsNum; j++)
			for (int k = 0; k < columnsNum; k++)
				result.rows[i][j] += rows[i][k] * matr.rows[k][j];
	return result;
}

Matrix& Matrix::operator=(const Matrix& matr)
{
	if (this != &matr)
	{
		if (rowsNum != matr.rowsNum || columnsNum != rowsNum)
		{
			if (rows)
				delete[] rows;
			rowsNum = matr.rowsNum;
			columnsNum = matr.columnsNum;
			rows = new Vector[rowsNum];
		}
		for (int i = 0; i < rowsNum; i++)
			rows[i] = matr.rows[i];
		return *this;
	}
}

Matrix Matrix::operator+(double num)
{
	Matrix result(rowsNum, columnsNum);
	for (int i = 0; i < rowsNum; i++)
		result.rows[i] = rows[i] + num;
	return result;
}

Matrix Matrix::operator-(double num)
{
	Matrix result(rowsNum, columnsNum);
	for (int i = 0; i < rowsNum; i++)
		result.rows[i] = rows[i] - num;
	return result;
}

Matrix Matrix::operator*(double num)
{
	Matrix result(rowsNum, columnsNum);
	for (int i = 0; i < rowsNum; i++)
		result.rows[i] = rows[i] * num;
	return result;
}

Matrix Matrix::operator/(double num)
{
	if (!num)
	{	
		cout << "Division by zero!" << endl;
		exit(EXIT_FAILURE);
	}
	Matrix result(rowsNum, columnsNum);
	for (int i = 0; i < rowsNum; i++)
		result.rows[i] = rows[i] / num;
	return result;
}

Matrix& Matrix::operator+=(const Matrix& matr)
{
	try
	{
		if (rowsNum != matr.rowsNum || columnsNum != matr.columnsNum)
		{
			DIF_SIZES exc(rowsNum, columnsNum, matr.rowsNum, matr.columnsNum);
			throw exc;
		}
	}
	catch (DIF_SIZES exc)
	{
		exc.info();
		exit(EXIT_FAILURE);
	}
	for (int i = 0; i < rowsNum; i++)
		rows[i] += matr.rows[i];
	return *this;
}

Matrix& Matrix::operator-=(const Matrix& matr)
{
	try
	{
		if (rowsNum != matr.rowsNum || columnsNum != matr.columnsNum)
		{
			DIF_SIZES exc(rowsNum, columnsNum, matr.rowsNum, matr.columnsNum);
			throw exc;
		}
	}
	catch (DIF_SIZES exc)
	{
		exc.info();
		exit(EXIT_FAILURE);
	}
	for (int i = 0; i < rowsNum; i++)
		rows[i] -= matr.rows[i];
	return *this;
}

Matrix& Matrix::operator+=(double num)
{
	for (int i = 0; i < rowsNum; i++)
		rows[i] += num;
	return *this;
}

Matrix& Matrix::operator-=(double num)
{
	for (int i = 0; i < rowsNum; i++)
		rows[i] -= num;
	return *this;
}

Matrix& Matrix::operator*=(double num)
{
	for (int i = 0; i < rowsNum; i++)
		rows[i] *= num;
	return *this;
}

Matrix& Matrix::operator/=(double num)
{
	if (!num)
	{
		cout << "Division by zero!" << endl;
		exit(EXIT_FAILURE);
	}
	for (int i = 0; i < rowsNum; i++)
		rows[i] /= num;
	return *this;
}

bool Matrix::operator==(const Matrix& matr) const
{
	if (rowsNum != matr.rowsNum || columnsNum != matr.columnsNum)
		return false;
	for (int i = 0; i < rowsNum; i++)
		if (rows[i] != matr.rows[i])
			return false;
	return true;
}

bool Matrix::operator!=(const Matrix& matr) const
{
	if (rowsNum != matr.rowsNum || columnsNum != matr.columnsNum)
		return true;
	for (int i = 0; i < rowsNum; i++)
		if (rows[i] != matr.rows[i])
			return true;
	return false;
}

Vector& Matrix::operator[](int index)
{
	try
	{
		if (index < 0 || index >= rowsNum)
		{
			OUT_OF_RANGE exc(index, rowsNum);
			throw exc;
		}
	}
	catch (OUT_OF_RANGE exc)
	{
		exc.info();
		exit(EXIT_FAILURE);
	}
	return rows[index];
}

const Vector& Matrix::operator[](int index) const
{
	try
	{
		if (index < 0 || index >= rowsNum)
		{
			OUT_OF_RANGE exc(index, rowsNum);
			throw exc;
		}
	}
	catch (OUT_OF_RANGE exc)
	{
		exc.info();
		exit(EXIT_FAILURE);
	}
	return rows[index];
}

double Matrix::find_determinant()
{
	if (rowsNum != columnsNum)
	{
		cout << "Determinant cannot be calculated: the matrix isn't square" << endl;
		exit(EXIT_FAILURE);
	}
	if (rowsNum == 1 && columnsNum == 1)
		return rows[0][0];
	if (rowsNum == 2 && columnsNum == 2)
		return rows[0][0] * rows[1][1] - rows[0][1] * rows[1][0];
	Matrix res = *this;
	if (!res.rows[0][0])
	{
		int i = 1;
		for (i; i < res.rowsNum; i++)
			if (res.rows[i][0])
			{
				res.rows[0] += res.rows[i];
				break;
			}
		if (i == res.rowsNum)
			return 0;
	}
	for (int i = 1; i < res.rowsNum; i++)
		res.rows[i] -= res.rows[0] * (res.rows[i][0] / res.rows[0][0]); 
	Matrix tmp(res.rowsNum - 1, res.columnsNum - 1);
	for (int i = 0; i < tmp.rowsNum; i++)
		for (int j = 0; j < tmp.columnsNum; j++)	
			tmp.rows[i][j] = res.rows[i + 1][j + 1];
	return res.rows[0][0] * tmp.find_determinant();
}

Matrix Matrix::transpose()
{
	Matrix result(columnsNum, rowsNum, 0);
	for (int i = 0; i < result.rowsNum; i++)
		for (int j = 0; j < result.columnsNum; j++)
			result.rows[i][j] = rows[j][i];
	return result;
}

Matrix Matrix::getWithoutRowAndColumn(int row, int column)
{
	if (row < 0 || row >= rowsNum || column < 0 || column >= columnsNum)
	{
		cout << "OUT OF RANGE!" << endl;
		exit(EXIT_FAILURE);
	}
	Matrix res(rowsNum - 1, columnsNum - 1);
	Vector tmp(res.columnsNum);
	int index = 0;
	int count = 0;
	for (int i = 0; i < rowsNum; i++)
	{
		if (i == row)
			continue;
		else
		{
			for (int j = 0; j < columnsNum; j++)
				if (j == column)
					continue;
				else
				{
					tmp[index] = rows[i][j];
					index++;
				}
			res.rows[count++] = tmp;
			index = 0;
		}
	}
	return res;
}

Matrix Matrix::invert()
{
	if (rowsNum != columnsNum)
	{
		cout << "Inverse matrix doesn't exist: the matrix isn't square" << endl;
		exit(EXIT_SUCCESS);
	}
	if (rowsNum == 1 && columnsNum == 1)
		return *this;
	double det = find_determinant();
	if (!det)
	{
		cout << "Inverse matrix doesn't exist: determinant = 0" << endl;
		exit(EXIT_SUCCESS);
	}
	Matrix result(rowsNum, columnsNum);	// Матрица из алгебраических дополнений.
	for (int i = 0; i < rowsNum; i++)
		for (int j = 0; j < columnsNum; j++)
			result[i][j] = pow((-1), i + j) * getWithoutRowAndColumn(i, j).find_determinant();
	return result.transpose() / det;
}

void Matrix::saveToFile()
{
	ofstream dataFile("data_out.txt");
	if (!dataFile.is_open())
	{
		cout << "Error while creating file \"data_out.txt\"" << endl;
		exit(EXIT_FAILURE);
	}
	dataFile << "MATRIX:\n" << endl;
	dataFile << *this << endl;
	dataFile << "\nDETERMINANT: " << fixed << find_determinant() << endl;
	dataFile.unsetf(ios::fixed);
	dataFile << "\nTRANSPOSED MATRIX:\n\n" << transpose() << endl;
	dataFile << "\nINVERSE MATRIX:\n\n" << invert() << endl;
	cout << "All changes have been saved. Check \"data_out.txt\"" << endl;
	dataFile.close();
}

Matrix Matrix::readFromFile()
{
	ifstream dataIn("data_in.txt");
	if (!dataIn.is_open())
	{
		cout << "Error while opening file \"data_in.txt\"" << endl;
		exit(EXIT_FAILURE);
	}

	string rowsNum, columnsNum, tmp;
	getline(dataIn, rowsNum);
	getline(dataIn, columnsNum);
	getline(dataIn, tmp);

	int nRows = atoi(rowsNum.c_str());
	int nColumns = atoi(columnsNum.c_str());

	Matrix res(nRows, nColumns, 0);
	for (int i = 0; i < nRows; i++)
		for (int j = 0; j < nColumns; j++)
			dataIn >> res.rows[i][j];
	return res;
}
