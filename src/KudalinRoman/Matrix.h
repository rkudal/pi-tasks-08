#pragma once
#include "Vector.h"
using namespace std;

class Matrix
{
private:
	int rowsNum;
	int columnsNum;
	Vector *rows;
	Matrix getWithoutRowAndColumn(int row, int column);  // ��������� ������� ��� ��������� ������ � �������.
public:
	Matrix();
	Matrix(int rowsNum, int columnsNum);
	Matrix(int rowsNum, int columnsNum, double filler);
	Matrix(const Matrix& matr);
	~Matrix();

	void random();	// ���������� ������� ���������� ������� �� 0 �� 100.

	friend ostream& operator<<(ostream& stream, const Matrix& matr);
	friend istream& operator>>(istream& stream, Matrix& matr);

	Matrix operator+(const Matrix& matr);
	Matrix operator-(const Matrix& matr);
	Matrix operator*(const Matrix& matr);
	Matrix& operator=(const Matrix& matr);
	Matrix operator+(double num);
	Matrix operator-(double num);
	Matrix operator*(double num);
	Matrix operator/(double num);

	Matrix& operator+=(const Matrix& matr);
	Matrix& operator-=(const Matrix& matr);
	Matrix& operator+=(double num);
	Matrix& operator-=(double num);
	Matrix& operator*=(double num);
	Matrix& operator/=(double num);

	bool operator==(const Matrix& matr) const;
	bool operator!=(const Matrix& matr) const;

	Vector& operator[](int index);
	const Vector& operator[](int index) const;

	double find_determinant();	// ����� ������������.
	Matrix transpose();	// ����������������.
	Matrix invert();	// ����� �������� �������.

	void saveToFile();	// ������ ������ � ����.
	Matrix readFromFile();	// ������ ������� �� �����.
};

ostream& operator<<(ostream& stream, const Matrix& matr);
istream& operator >> (istream& stream, Matrix& matr);