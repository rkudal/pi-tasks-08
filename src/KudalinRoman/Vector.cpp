#include "Vector.h"
#include "Exceptions.h"
#include <iomanip>
using namespace std;

Vector::Vector()
{
	count = 0;
	mem = nullptr;
}

Vector::Vector(int count)
{
	this->count = count;
	mem = new double[count];
}

Vector::Vector(int count, double filler)
{
	this->count = count;
	mem = new double[count];
	for (int i = 0; i < count; i++)
		mem[i] = filler;
}

Vector::Vector(const Vector& vec)
{
	count = vec.count;
	mem = new double[count];
	for (int i = 0; i < count; i++)
		mem[i] = vec.mem[i];
}

Vector::~Vector()
{
	if (mem)
		delete[] mem;
	mem = nullptr;
}

Vector Vector::operator+(const Vector& vec)
{
	try
	{
		if (count != vec.count)
		{
			DIF_LENGTH exc(count, vec.count);
			throw exc;
		}
	}
	catch (DIF_LENGTH exc)
	{
		exc.info();
		exit(EXIT_FAILURE);
	}
	Vector result(count);
	for (int i = 0; i < count; i++)
		result.mem[i] = mem[i] + vec.mem[i];
	return result;
}

Vector Vector::operator-(const Vector& vec)
{
	try
	{
		if (count != vec.count)
		{
			DIF_LENGTH exc(count, vec.count);
			throw exc;
		}
	}
	catch (DIF_LENGTH exc)
	{
		exc.info();
		exit(EXIT_FAILURE);
	}
	Vector result(count);
	for (int i = 0; i < count; i++)
		result.mem[i] = mem[i] - vec.mem[i];
	return result;
}

Vector& Vector::operator=(const Vector& vec)
{
	if (this != &vec)
	{
		if (count != vec.count)
		{
			if (mem)
				delete[] mem;
			count = vec.count;
			mem = new double[count];
		}
		for (int i = 0; i < count; i++)
			mem[i] = vec.mem[i];
		return *this;
	}
}

Vector Vector::operator+(double num)
{
	Vector result(count);
	for (int i = 0; i < count; i++)
		result.mem[i] = mem[i] + num;
	return result;
}

Vector Vector::operator-(double num)
{
	Vector result(count);
	for (int i = 0; i < count; i++)
		result.mem[i] = mem[i] - num;
	return result;
}

Vector Vector::operator*(double num)
{
	Vector result(count);
	for (int i = 0; i < count; i++)
		result.mem[i] = mem[i] * num;
	return result;
}

Vector Vector::operator/(double num)
{
	if (!num)
	{
		cout << "Division by zero!" << endl;
		exit(EXIT_FAILURE);
	}
	Vector result(count);
	for (int i = 0; i < count; i++)
		result.mem[i] = mem[i] / num;
	return result;
}

Vector& Vector::operator+=(const Vector& vec)
{
	try
	{
		if (count != vec.count)
		{
			DIF_LENGTH exc(count, vec.count);
			throw exc;
		}
	}
	catch (DIF_LENGTH exc)
	{
		exc.info();
		exit(EXIT_FAILURE);
	}
	for (int i = 0; i < count; i++)
		mem[i] += vec.mem[i];
	return *this;
}

Vector& Vector::operator-=(const Vector& vec)
{
	try
	{
		if (count != vec.count)
		{
			DIF_LENGTH exc(count, vec.count);
			throw exc;
		}
	}
	catch (DIF_LENGTH exc)
	{
		exc.info();
		exit(EXIT_FAILURE);
	}
	for (int i = 0; i < count; i++)
		mem[i] -= vec.mem[i];
	return *this;
}

Vector& Vector::operator+=(double num)
{
	for (int i = 0; i < count; i++)
		mem[i] += num;
	return *this;
}

Vector& Vector::operator-=(double num)
{
	for (int i = 0; i < count; i++)
		mem[i] -= num;
	return *this;
}

Vector& Vector::operator*=(double num)
{
	for (int i = 0; i < count; i++)
		mem[i] *= num;
	return *this;
}

Vector& Vector::operator/=(double num)
{
	if (!num)
	{
		cout << "Division by zero!" << endl;
		exit(EXIT_FAILURE);
	}
	for (int i = 0; i < count; i++)
		mem[i] /= num;
	return *this;
}

bool Vector::operator==(const Vector& vec) const
{
	if (count != vec.count)
		return false;
	for (int i = 0; i < count; i++)
		if (mem[i] != vec.mem[i])
			return false;
	return true;
}

bool Vector::operator!=(const Vector& vec) const
{
	if (count != vec.count)
		return true;
	for (int i = 0; i < count; i++)
		if (mem[i] != vec.mem[i])
			return true;
	return false;
}

ostream& operator<<(ostream& stream, const Vector& vec)
{
	for (int i = 0; i < vec.count; i++)
	{
		stream << setfill(' ') << setw(13) << left;
		stream << vec.mem[i];
	}
	stream << endl;
	return stream;
}

istream& operator>>(istream& stream, Vector& vec)
{
	cout << "Enter " << vec.count << " vectors elements:" << endl;
	for (int i = 0; i < vec.count; i++)
		stream >> vec[i];
	return stream;
}

double& Vector::operator[](int index)
{
	try
	{
		if (index < 0 || index >= count)
		{
			OUT_OF_RANGE exc(index, count);
			throw exc;
		}
	}
	catch (OUT_OF_RANGE exc)
	{
		exc.info();
		exit(EXIT_FAILURE);
	}
	return mem[index];
}

const double& Vector::operator[](int index) const
{
	try
	{
		if (index < 0 || index >= count)
		{
			OUT_OF_RANGE exc(index, count);
			throw exc;
		}
	}
	catch (OUT_OF_RANGE exc)
	{
		exc.info();
		exit(EXIT_FAILURE);
	}
	return mem[index];
}