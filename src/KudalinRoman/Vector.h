#pragma once
#include <iostream>
using namespace std;

class Vector
{
private:
	double *mem;
	int count;
public:
	Vector();
	explicit Vector(int count);
	Vector(int count, double filler);
	Vector(const Vector& vec);
	~Vector();

	Vector operator+(const Vector& vec);
	Vector operator-(const Vector& vec);
	Vector& operator=(const Vector& vec);
	Vector operator+(double num);
	Vector operator-(double num);
	Vector operator*(double num);
	Vector operator/(double num);

	Vector& operator+=(const Vector& vec);
	Vector& operator-=(const Vector& vec);
	Vector& operator+=(double num);
	Vector& operator-=(double num);
	Vector& operator*=(double num);
	Vector& operator/=(double num);

	bool operator==(const Vector& vec) const;
	bool operator!=(const Vector& vec) const;

	friend ostream& operator<<(ostream& stream, const Vector& vec);
	friend istream& operator>>(istream& stream, Vector& vec);

	double& operator[](int index);
	const double& operator[](int index) const;
};

ostream& operator<<(ostream& stream, const Vector& vec);
istream& operator >> (istream& stream, Vector& vec);