#include "Vector.h"
#include "Matrix.h"
#include <ctime>
using namespace std;

int main()
{
	Matrix res, m(5, 5);
	srand(time(0));
	res = res.readFromFile();
	cout << "FROM THE FILE:\n\n";
	cout << res << endl;
	cout << "Determinant: " << fixed << res.find_determinant() << endl;
	cout.unsetf(ios::fixed);
	cout << "\nTransposed:\n\n" << res.transpose() << endl;
	cout << "Inverse:\n\n" << res.invert() << endl;


	m.random();
	cout << "----------------------MATRIX M:------------------------\n\n";
	cout << m << endl;
	cout << "Determinant: " << fixed << m.find_determinant() << endl;
	cout.unsetf(ios::fixed);
	cout << "\nTransposed:\n\n" << m.transpose() << endl;
	cout << "Inverse:\n\n" << m.invert() << endl;
	m.saveToFile();
	cout << "-------------------------------------------------------" << endl;


	Matrix m1(5, 5);
	Matrix m2(5, 5);
	m1.random();
	m2.random();
	cout << "\n---------------MATRIX m1----------MATRIX m2------------\n" << endl;
	cout << "\nm1:\n" << endl << m1 << endl << "\nm2:\n" << endl << m2 << endl;
	cout << "\n----------------------------m1 + m2---------------------\n" << endl;
	cout << m1 + m2 << endl;
	cout << "\n----------------------------m1 - m2---------------------\n" << endl;
	cout << m1 - m2 << endl;
	cout << "\n----------------------------m1 * m2---------------------\n" << endl;
	cout << m1 * m2 << endl;
	cout << "------------------------------------------------------------" << endl;

	return 0;
}